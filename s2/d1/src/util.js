function factorial(n){
    // if(typeof n !== 'number') return undefined; // will use in s04
    // if(n<0) return undefined; // will use in s04
    if(n===0) return 1;
    if(n===1) return 1;
    return n * factorial(n-1);
    // 4 * 3 * 2 * 1
}


function div_check(number) {
    if (number % 5 === 0 || number % 7 === 0) {
        return true;
    } else {
        return false;
    }
}

module.exports = {
    factorial: factorial,
    div_check: div_check
};
